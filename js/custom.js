jQuery(document).ready(function () {

    //Wow
    wow = new WOW({

        mobile: false // default
    })
    wow.init();

    //Sticky Header
    jQuery(window).scroll(function () {
        var scroll = jQuery(window).scrollTop();

        if (scroll >= 200) {
            jQuery(".header").addClass("fixed-header");
        } else {
            jQuery(".header").removeClass("fixed-header");
        }
    });
});

 //Preloader
 jQuery(window).on('load',function () {
    jQuery('.preloader').remove();
});
    
//Paraleex

  if(navigator.userAgent.match(/Trident\/7\./)) {
  document.body.addEventListener("mousewheel", function() {
    event.preventDefault();
    var wd = event.wheelDelta;
    var csp = window.pageYOffset;
    window.scrollTo(0, csp - wd);
  });
}

//Textarea 

jQuery(function (jQuery) {
    jQuery('.firstCap, textarea').on('keypress', function (event) {
        var jQuerythis = jQuery(this),
            thisVal = jQuerythis.val(),
            FLC = thisVal.slice(0, 1).toUpperCase();
        con = thisVal.slice(1, thisVal.length);
        jQuery(this).val(FLC + con);
    });
});

//Page Zoom

document.documentElement.addEventListener('touchstart', function (event) {
 if (event.touches.length > 1) {
   event.preventDefault();
 }
}, false);


/*
technique described here: https://css-tricks.com/svg-line-animation-works/
I added GSAP to the recipe :)
*/
TweenMax.to(document.querySelector('.loading'), 4, {
  attr:{'stroke-dashoffset': 0},
  ease: Sine.easeInOut,
  repeat: -1,
  yoyo: true
});

TweenMax.to(document.querySelector('.loading1'), 4, {
  attr:{'stroke-dashoffset': 0},
  ease: Sine.easeInOut,
  repeat: -1,
  yoyo: true
});

TweenMax.to(document.querySelector('.loading2'), 4, {
  attr:{'stroke-dashoffset': 0},
  ease: Sine.easeInOut,
  repeat: -1,
  yoyo: true
});
